﻿using System;
using System.Collections.Generic;


namespace Classess
{
    public class PetShop
    {
       
        ICollection<Pet> Pets;

        public PetShop()
        {
            Pets = new List<Pet>();
        }

       
         public void IntroduceAll()
        {
            foreach (Pet pet in Pets)
            {
                pet.Introduce();
            }
        }

        public void AddPet(Pet newPet)
        {
              
            if (newPet == null)
            {
                throw new Exception("Pet is null.");
            }

            
                if (Pets.Contains(newPet))  // became effective after Pet.Equals overriding :-)
                {
                    throw new Exception("Pet is already here.");
                }


                Pets.Add(newPet);
         }
    }
}
