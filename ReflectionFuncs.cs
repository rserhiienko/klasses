﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Classess
{
    static class ReflectionFuncs
    {
        public static IEnumerable<Type> GetDerivedTypesFor(Type baseType)
        {
            var assembly = Assembly.GetExecutingAssembly();

            return assembly.GetTypes().Where(baseType.IsAssignableFrom).Where(t => baseType != t);
        }

        public static object CreatePetInstance(string className, string name, string breed)
        {
            var assembly = Assembly.GetExecutingAssembly();

            var type = assembly.GetTypes().First(t => t.Name == className);

            return Activator.CreateInstance(type, name, breed);
        }
    }
}
