﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classess
{
    class ConsolePetInput
    {
        public static string DerivClList() {
            IEnumerable<Type> derivedClassesList = ReflectionFuncs.GetDerivedTypesFor(typeof(Pet));
            string classesList = "";
            foreach (Type temp in derivedClassesList)
            {
                classesList += (temp.Name + ", ");
            }
            if (classesList.Length > 2) classesList = classesList.Remove(classesList.Length - 2);
            return classesList; }

        public static Pet StringToPetConv(string triplet)
        {
            string[] family_name_breed = triplet.Split(' '); ;
            if (family_name_breed.Length < 3)         {
                throw new Exception("Some fields have the null value");
                }
                string family = family_name_breed[0];
                string name = family_name_breed[1];
                string breed = family_name_breed[2];    

            object temp = ReflectionFuncs.CreatePetInstance(family_name_breed[0], family_name_breed[1], family_name_breed[2]);

            return (Pet)temp;
        }
    }
}
