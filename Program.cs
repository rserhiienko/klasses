﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classess
{

    class Program
    {

        static void Main(string[] args)
        {
            bool addNextPet = true;
            PetShop petShop = new PetShop();

            string derivedClassesList = ConsolePetInput.DerivClList();

            while (addNextPet)
            {
                Console.Write("Enter pet family ({0}), pet name and pet breed, space separated (\"exit\" to exit): ", derivedClassesList);
                string petData = Console.ReadLine();
                if (petData == null || petData == "" || petData == "exit")
                {
                    addNextPet = false;
                    break;
                } 
                Pet newPet = ConsolePetInput.StringToPetConv(petData);
                petShop.AddPet(newPet);
            }

            Console.WriteLine("All the pets:");
            petShop.IntroduceAll();
             
            Console.ReadLine();
        } 
    }
}
