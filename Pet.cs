﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classess
{
    public class Pet
    {
        public string Name { get; private set; }
        public string Breed { get; private set; }

        
        public Pet(string name, string breed) {
            if (name == null || name.Length == 0) {
                throw new Exception("name is null or empty");  }
            if (breed == null || breed.Length == 0) {
                throw new Exception("breed is null or empty");  }
            Name = name;
            Breed = breed;
        }

        protected virtual string Family { get { return GetType().Name.ToLower(); } }

        public virtual void Introduce() {
            Console.WriteLine("I'm " + Name + " of " + Breed + ". I'm a " + Family + ".");
        }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Pet p = (Pet)obj;
                return (Name == p.Name) && (Breed == p.Breed) && (Family == p.Family);
            }
        }

        public override int GetHashCode()
        {
            return (Name + Breed +Family).GetHashCode();

        }



    }
}
