## PetShop application
Classes Pet, Petshop were created. Classes Cat, Dog inherit Pet class.
Types Cat and Dog have fields Name and Breed and method Introduce() which prints text �I�m (Name) of (Breed). I�m a cat (or dog)�.
The class Petshop collects in its container different pets.
We may add new pet to container by method AddPet() and we may display information about all pets by method IntroduceAll().
Which hierarchy of classes is the best solution of this problem?
# Features
When enter a pet, you will be provided with available classes inherit form Pet. First you should enter class name, 
which corresponds to animal family, then name and breed. ("cat Tom mainecoon"). Method Equals() was overriden so it became possible to use Contains() method 
to avoid entering existing pair "name+breed"
Code includes class (interface) aggregation, inheritance, uses .NET BCL collections or generics, and implements exception handling.
